import React, { Component } from 'react';
import CardItem from '../Cartitem'

class Cards extends Component {
    render() {
        return (
            <div className='row'>
                <div className='col-sm-4'>
                    <CardItem/>
                </div>

                <div className='col-sm-4'>
                    <CardItem/>
                </div>

                <div className='col-sm-4'>
                    <CardItem/>
                </div>
            </div>
        );
    }
}

export default Cards;