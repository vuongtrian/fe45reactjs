import React, { Component } from "react";
import Header from "../Header";
import Carousel from "../Carousel";
import Intro from "../Intro";
import Cards from "../Cards";
import Footer from "../Footer";

class Home extends Component {
  render() {
    return (
      <div className="Home">
        <Header />
        <Carousel />
        <div className="container">
          <Intro />
          <Cards />
        </div>
        <Footer/>
      </div>
    );
  }
}

export default Home;
