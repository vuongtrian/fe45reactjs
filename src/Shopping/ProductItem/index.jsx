import React, { Component } from "react";
import classes from "../ProductItem/style.module.css";

class ProductItem extends Component {
  state = {
    isShow: true,
  };

  toggleDesc = () => {
    this.setState({
      isShow: !this.state.isShow,
    });
  };

  render() {
    //Dùng kỹ thuật bóc tách phần tử của ES6
    const { name, img, desc } = this.props.item;

    return (
      <div className="card">
        <img src={img} alt="product" className={classes.productImg} />
        <div className="card-body">
          <h3>{name}</h3>

          {this.state.isShow && <p>{desc}</p>}

          {/* {this.isShow ? <p>{desc}</p> : null} */}

          <button className="btn btn-success mx-2" onClick={() => this.props.getProduct(this.props.item)}>Xem chi tiết</button>
          <button className="btn btn-info" onClick = {this.toggleDesc}>Ẩn mô tả</button>
          <button className="btn btn-warning" onClick={() => this.props.putToCart(this.props.item)}>Giỏ hàng</button>
        </div>
      </div>
    );
  }
}

export default ProductItem;
