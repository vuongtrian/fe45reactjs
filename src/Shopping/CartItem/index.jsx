import React, { Component } from 'react';

class CartItem extends Component {
    render() {

        const{img, name, price} = this.props.item.product;
        const {quantity} = this.props.item;

        return (
            <div>
                <tr>
                        <td>
                          <img
                            style={{ width: 200 }}
                            src={img}
                          />
                        </td>
                        <td style={{ fontSize: 25 }}>{name}</td>
                        <td>{price}</td>
                        <td>
                          {quantity}
                          <div className="btn-group">
                            <button className="btn btn-info border-right">
                              -
                            </button>
                            <button className="btn btn-info border-left">
                              +
                            </button>
                          </div>
                        </td>
                        <td>{quantity * price}</td>
                        <td>
                          <button className="btn btn-info">x</button>
                        </td>
                      </tr>
            </div>
        );
    }
}

export default CartItem;