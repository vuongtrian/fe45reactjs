import React, { Component } from "react";

class Databinding extends Component {
  name = "Vương Trị An";
  isMale = true;

  checkGender() {
    if (this.isMale) return "Male";
    return "Female";
  }

  //Trường hợp 1: hàm bình thường, không tham số
  showMessage() {
    console.log("test");
  }

  // Trường hợp 2: hàm nhận tham số đầu vào
  //   showMessageWithParams(message) {
  //     return function () {
  //       console.log(message);
  //     };
  //   }
  showMessageWithParams = (message) => () => {
    console.log(message);
  };

  //Trường hợp 3: hàm có con trỏ this
  showMessageWithThis = () => {
      console.log(this.name);
  }


  render() {
    let age = 25;
    return (
      <div>
        {/* Trong sự kiện onClick, khi truyền vào hàm thì không bỏ ngoặc () vào */}
        <button onClick={this.showMessage}>Show message</button>

        <button onClick={this.showMessageWithThis}>Show message with this</button>

        <button onClick={this.showMessageWithParams("An")}>
          Show message with params
        </button>

        <h1>Demo databinding</h1>
        <h3>Author: {this.name}</h3>
        {/* <h3>Gender: {this.checkGender()}</h3> */}
        {/* Toán tử 3 ngôi */}
        <h3>Gender: {this.isMale ? 'Male' : 'Female'}</h3>
        <h3>Age: {age}</h3>
      </div>
    );
  }
}

export default Databinding;
