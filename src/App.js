import React from "react";
// import logo from './logo.svg';
// import Header from "./header"
// import Footer from './footer'
// import Home from './excercise-1/Home';
// import Home from './excercise-2/Home'
// import "./App.css";
// import Databinding from './Databiding';
// import CarExercise from './CarExercise';
// import MovieExercise from './MovieExercise';
// import DemoParent from './DemoProps/DemoParent';
// import HomeBaiTapReact_1 from './ReactJS_WebDienThoai/Home';
// import Glasse from "./ReactJS_WebThuKinh";
// import HomeShopping from "./Shopping/Home";
// import ReactForm from "./ReactForm";
// import PhoneHome from "./ReactJS_Phone/PhoneHome";
// import Home from "./UserManagerment/Home";
// import Home_Rework from "./UserManagerment_Rework/Home";
import FormValidation from "./Form-Validation";
// import LifeCycle from "./life-cycle";
// import FormValidation_Rework from "./Form-Validation_Rework";

function App() {
  return (
    <div className="App">
      {/* <Home/> */}
      {/* <Databinding/> */}
      {/* <CarExercise/> */}
      {/* <MovieExercise/> */}
      {/* <DemoParent/> */}
      {/* <HomeBaiTapReact_1/> */}
      {/* <Glasse /> */}
      {/* <HomeShopping/> */}
      {/* <ReactForm/> */}
      {/* <PhoneHome/> */}
      {/* <Home/> */}
      {/* <Home_Rework/> */}
      <FormValidation/>
      {/* <LifeCycle/> */}
      {/* <FormValidation_Rework/> */}
    </div>
  );
}

export default App;
