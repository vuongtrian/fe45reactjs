import React, { Component } from "react";
import Child from "./child";
import Pure from "./pure"

class LifeCycle extends Component {
  constructor(props) {
    super(props);

    console.log("constructor");

    this.state = {
      number: 0,
    };
  }

  UNSAFE_componentWillMount() {
    console.log("componentWillMount");
  }

  componentDidMount() {
    /**
     * Gọi api fetch data
     * - Chạy 1 lần duy nhất
     */
    console.log("componentDidMount");
  }

  componentDidUpdate() {
    /**
     * Chạy khi state thay đổi
     */
    console.log("componentDidUpdate");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("shouldComponentUpdate", nextProps, nextState);
    if (nextState.number === 3) {
      return false;
    }
    return true;
  }

  //   UNSAFE_componentWillReceiveProps(nextProps) {
  //       /**
  //        * sẽ chạy trong component con được truyền thông qua props
  //        */
  //     console.log("componentWillReceiveProps", nextProps);
  //   }

  static getDerivedStateFromProps(nextProps, currentState) {
    /**
     * thay thế cho componentWillReceiveProps
     */
    console.log("Child-getDerivedStateFromProps", nextProps, currentState);
    return {
      username: "Cybersoft",
    };
    //   return null;
  }

  handleClick = () => {
    this.setState({
      number: this.state.number + 1,
    });
  };

  render() {
    console.log("render");
    return (
      <div>
        <h3>Life Cycle</h3>
        <p>Number: {this.state.number}</p>
        <button className="btn btn-success" onClick={this.handleClick}>
          Click
        </button>
        <Child number={this.state.number} />
        <p>Username: {this.state.username}</p>
        <Pure/>
      </div>
    );
  }
}

export default LifeCycle;
