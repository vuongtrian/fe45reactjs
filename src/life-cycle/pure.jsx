import React, { PureComponent } from "react";

class Pure extends PureComponent {
  render() {
    console.log("Pure - render");
    return (
      <div>
        <h3>Pure</h3>
      </div>
    );
  }
}

export default Pure;
