import React, { Component } from "react";

class child extends Component {

    

    UNSAFE_componentWillReceiveProps(nextProps) {
    console.log("Child - componentWillReceiveProps", nextProps);
  }

  render() {
    console.log("Child-render");
    return (
      <div>
        <h3>Child</h3>
      </div>
    );
  }
}

export default child;
