import React, { Component } from "react";

class PhoneItem extends Component {
  render() {

    let {phone, addCart} = this.props;

    return (
      <div className="col-4">
        <div className="card">
          <img
            className="card-img-top"
            src={phone.img}
            alt="Cellphone Image"
            width={300}
            height={350}
          />
          <div className="card-body">
            <h4 className="card-title">{phone.name}</h4>
            <button
              className="btn btn-success"
              onClick={() => this.props.showDetail(phone)}
            >
              Detail
            </button>
            <button className="btn btn-danger" onClick={() => addCart(phone)}>Add Cart</button>
          </div>
        </div>
      </div>
    );
  }
}

export default PhoneItem;
