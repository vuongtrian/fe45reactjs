import React, { Component } from "react";
import PhoneItem from "../PhoneItem";

class PhoneList extends Component {
  state = {
    detailProduct: this.props.products[0],
  };

  renderProduct = () => {
    return this.props.products.map((phone, index) => {
      return (
        <PhoneItem addCart={this.props.addCart} key={index} phone={phone} showDetail={this.showDetail}/>
      );
    });
  };

//   Xử lý nút Detail
showDetail = (phone) => {
    this.setState({
        detailProduct: phone,
    })
}

  render() {
    let { detailProduct } = this.state;

    return (
      <div className="container">
        <div className="row">{this.renderProduct()}</div>
        <div className="row">
          <div className="col-6">
            <h3 className="text-align-center">{detailProduct.name}</h3>
            <img
              src={detailProduct.img}
              alt="Product Image"
              width={300}
              height={300}
            />
          </div>
          <div className="col-6">
            <h3>Specification</h3>
            <table className="table">
              <tr>
                <th>Screen</th>
                <th>{detailProduct.screen}</th>
              </tr>
              <tr>
                <th>Operating System</th>
                <th>{detailProduct.system}</th>
              </tr>
              <tr>
                <th>Front Camera</th>
                <th>{detailProduct.frontCamera}</th>
              </tr>
              <tr>
                <th>Back Camera</th>
                <th>{detailProduct.backCamera}</th>
              </tr>
              <tr>
                <th>Price</th>
                <th>{detailProduct.price}</th>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default PhoneList;
