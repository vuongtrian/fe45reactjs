import React, { Component } from "react";

class PhoneCartList extends Component {
  render() {
    const { phoneCart, deleteCart, adjustQty } = this.props;

    return (
      <div>
        <div
          className="modal fade"
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div
              className="modal-content"
              style={{ maxWidth: "800px", width: "800px" }}
            >
              <div className="modal-header">
                <h5 className="modal-title">Cart</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>Number</th>
                      <th>Photo</th>
                      <th>Name</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Amount</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {phoneCart.map((phoneCartAdd, index) => {
                      return (
                        <tr key={index}>
                          <td>{phoneCartAdd.id}</td>
                          <td>
                            <img
                              src={phoneCartAdd.img}
                              alt="Phone"
                              width={75}
                              height={75}
                            />
                          </td>
                          <td>{phoneCartAdd.name}</td>
                          <td>
                            <button className="btn btn-success" onClick={() => adjustQty(phoneCartAdd.id, true)}>+</button>
                            {phoneCartAdd.Qty}
                            <button className="btn btn-success" onClick={() => adjustQty(phoneCartAdd.id, false)}>-</button>
                            </td>
                          <td>{phoneCartAdd.price}</td>
                          <td>{phoneCartAdd.Qty * phoneCartAdd.price}</td>
                          <td>
                            <button
                              className="btn btn-danger"
                              onClick={() => deleteCart(phoneCartAdd.id)}
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PhoneCartList;
