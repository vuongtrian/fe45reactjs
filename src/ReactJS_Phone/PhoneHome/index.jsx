import React, { Component } from "react";
import PhoneList from "../PhoneList";
import PhoneCartList from "../PhoneCartList";

class PhoneHome extends Component {
  products = [
    {
      id: "sp_1",
      name: "iphone11",
      price: 400,
      screen: "6.1 inches",
      backCamera: "Camera kép 12MP",
      frontCamera: "12 MP, ƒ/2.2 aperture",
      system: "IOS",
      img:
        "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone11-purple-select-2019.png",
      desc:
        "iPhone 11 features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      id: "sp_2",
      name: "Samsung Note 20 Ultra",
      price: 500,
      screen: "6.9 inches",
      backCamera: "backCamera_2",
      frontCamera: "10 MP, f/2.2, 26mm (wide), 1/3.2, 1.22µm, Dual Pixel PDAF",
      system: "Android",
      img:
        "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/b/l/black_final.jpg",
      desc:
        "The Galaxy Note20 Ultra comes with a perfectly symmetrical design for good reason",
    },
    {
      id: "sp_3",
      name: "Vivo",
      price: 200,
      screen: "screen_3",
      backCamera: "backCamera_3",
      frontCamera: "frontCamera_3",
      system: "Android",
      img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
      desc:
        "A young global smartphone brand focusing on introducing perfect sound quality",
    },
    {
      id: "sp_4",
      name: "Blacberry",
      price: 250,
      screen: "screen_4",
      backCamera: "backCamera_4",
      frontCamera: "frontCamera_4",
      system: "Android",
      img:
        "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
      desc:
        "BlackBerry is a line of smartphones, tablets, and services originally designed",
    },
    {
      id: "sp_5",
      name: "Apple iPhone XR",
      price: 452,
      screen: "screen_5",
      backCamera: "backCamera_5",
      frontCamera: "frontCamera_5",
      system: "IOS",
      img:
        "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone_xr_128gb.jpg",
      desc:
        "The iPhone XR delivers fast performance, great cameras and long battery life for a good price.",
    },
    {
      id: "sp_6",
      name: "Google - Pixel 3a",
      price: 452,
      screen: "screen_6",
      backCamera: "backCamera_6",
      frontCamera: "frontCamera_6",
      system: "Android",
      img:
        "https://cdn.tgdd.vn/Products/Images/42/200596/google-pixel-3a-400x460.png",
      desc:
        "Bring out all the best details and colors of your low light photos with Night Sight on Pixel 3a. You'll never want to use flash again",
    },
  ];

  state = {
    phoneCart: [
      {
        id: "sp_1",
        name: "iphone11",
        price: 400,
        img:
          "https://cdn.cellphones.com.vn/media/catalog/product/cache/7/image/9df78eab33525d08d6e5fb8d27136e95/i/p/iphone11-purple-select-2019.png",
        Qty: 1,
      },
    ],
  };

  addCart = (selectedPhone) => {
    let phoneInCart = {
      id: selectedPhone.id,
      name: selectedPhone.name,
      price: selectedPhone.price,
      img: selectedPhone.img,
      Qty: 1,
    };

    var updatedCart = [...this.state.phoneCart];
    let index = updatedCart.findIndex((phone) => phone.id === phoneInCart.id);
    if (index !== -1) {
      updatedCart[index].Qty += 1;
    } else {
      updatedCart.push(phoneInCart);
    }

    this.setState({
      phoneCart: updatedCart,
    });
  };

  deleteCart = (phoneId) => {
    var updatedCart = [...this.state.phoneCart];
    let index = updatedCart.findIndex (phone => phone.id === phoneId);
    if (index !== -1)
    {
      updatedCart.splice(index,1);
    }
    this.setState({
      phoneCart: updatedCart,
    })
  }

  adjustQty = (phoneId, change) => {
    var updatedCart = [...this.state.phoneCart];
    let index = updatedCart.findIndex (phone => phone.id === phoneId);
    if (change){
      updatedCart[index].Qty += 1;
    } else {
      if (updatedCart[index].Qty > 1){
        updatedCart[index].Qty -= 1;
      }
    }
    this.setState({
      phoneCart: updatedCart,
    })
  }

  render() {
    let totalQty = this.state.phoneCart.reduce((totalQty, phoneItem, index) => {
      return (totalQty += phoneItem.Qty);
    }, 0);

    return (
      <div className="container">
        <h3 className="text-center text-success">A-CELLPHONE</h3>
        <div className="text-right">
          <span
            className="text-danger"
            style={{ cursor: "pointer", fontSize: "17px", fontWeight: "bold" }}
            data-toggle="modal"
            data-target="#modelId"
          >
            Cart({totalQty})
          </span>
        </div>
        <PhoneList addCart={this.addCart} products={this.products} />
        <PhoneCartList phoneCart={this.state.phoneCart} deleteCart={this.deleteCart} adjustQty={this.adjustQty}/>
      </div>
    );
  }
}

export default PhoneHome;
