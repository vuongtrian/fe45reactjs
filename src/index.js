import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
// import formReducer from "./UserManagerment/redux/reducer/form";
// import userList from "./UserManagerment/redux/reducer/userList";
// import selectedUser from "./UserManagerment/redux/reducer/selectedUser";
// import formReducer from "./UserManagerment_Rework/redux/form";
// import UserList from "./UserManagerment_Rework/redux/userList";
// import selectedUser from "./UserManagerment_Rework/redux/selectedUser";
import formValidationReducer from "./Form-Validation/module/reducer";

// Tạo reducer tổng
const reducer = combineReducers({
  // Đây là nơi lưu trữ tất cả dữ liệu của store
  // tenDuLieu: reducerQuanLy
  // cart: cartReducer
  // isShow: formReducer,
  // UserList,
  // selectedUser, 
  formValidationReducer,
});

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
