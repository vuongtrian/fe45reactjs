import React, { Component } from 'react';

class DemoGrandChild extends Component {
    render() {
        console.log(this.props.item);
        return (
            <div className='bg-primary text-white p-3'>
                <h1>Demo GrandChild</h1>
                <p>Name: {this.props.item.name}</p>
            </div>
        );
    }
}

export default DemoGrandChild;