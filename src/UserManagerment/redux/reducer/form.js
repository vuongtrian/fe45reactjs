let initialState = false;

const reducer = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case "SHOW_FORM":
      // state = true;
      // return state;
      return true;
    case "CLOSE_FORM":
      // state = false;
      // return state;
      return false;
    default:
      return state;
  }
};

export default reducer;
