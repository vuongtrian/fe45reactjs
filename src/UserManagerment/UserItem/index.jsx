import React, { Component } from "react";
import { connect } from "react-redux";

class UserItem extends Component {
  handleDelete = () => {
    this.props.dispatch({
      type: "DELETE_USER",
      payload: this.props.item,
    });
  };

  OpenForm = () => {
    this.props.dispatch({
      type: "SHOW_FORM",
    });
  };

  handleEditUser = () => {
    this.OpenForm();
    this.props.dispatch({
      type: "SELECTED_USER",
      payload: this.props.item,
    });
  };

  render() {
    const { name, username, email, phoneNumber, type } = this.props.item;
    return (
      <tr>
        <td>{name}</td>
        <td>{username}</td>
        <td>{email}</td>
        <td>{phoneNumber}</td>
        <td>{type}</td>
        <td>
          <button onClick={this.handleEditUser} className="btn btn-info mr-2">
            Edit
          </button>
          <button onClick={this.handleDelete} className="btn btn-danger">
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default connect()(UserItem);
