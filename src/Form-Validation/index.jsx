import React, { Component } from "react";
import ListUser from "./list-user";
import { connect } from "react-redux";

class FormValidation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {
        manv: "",
        tennv: "",
        email: "",
      },
      errors: {
        manv: "",
        tennv: "",
        email: "",
      },
      manvValid: false,
      tennvValid: false,
      emailValid: false,
      formValid: false,
    };
  }

  handleOnChange = (e) => {
    const { name, value } = e.target;
    this.setState(
      {
        value: { ...this.state.value, [name]: value },
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleErrors = (e) => {
    //   console.log('handleErrors');
    const { name, value } = e.target;
    let mess = "";
    // if (value === "") {
    //   mess = "Khong duoc rong";
    // } else {
    //   mess = "";
    // }
    mess = value === "" ? name + " Khong duoc rong" : "";
    let { manvValid, tennvValid, emailValid } = this.state;
    switch (name) {
      case "manv":
        manvValid = mess !== "" ? false : true;
        if (value !== "" && value.length < 4) {
          manvValid = false;
          mess = "Ít nhất 4 ký tự";
        }
        break;
      case "tennv":
        tennvValid = mess !== "" ? false : true;
        break;
      case "email":
        emailValid = mess !== "" ? false : true;
        if (
          value !== "" &&
          !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
        ) {
          mess = "Email khong hop le";
          emailValid = true;
        }
        break;
      default:
        break;
    }

    this.setState(
      {
        errors: { ...this.state.errors, [name]: mess },
        manvValid,
        tennvValid,
        emailValid,
      },
      () => {
        this.validationFrom();
        console.log(this.state);
      }
    );
  };

  validationFrom = () => {
    const { manvValid, tennvValid, emailValid } = this.state;
    this.setState({
      formValid: manvValid && tennvValid && emailValid,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.props.value) {
      this.props.dispatch({
        type: "UPDATE_EMP",
        payload: this.state.value,
      });
    } else {
      this.props.dispatch({
        type: "ADD_EMP",
        payload: this.state.value,
      });
    }
  };

  render() {
    return (
      <div className="container">
        <h3 className="title">*FormValidation</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Mã nhân viên</label>
            <input
              type="text"
              className="form-control"
              name="manv"
              onChange={this.handleOnChange}
              onBlur={this.handleErrors}
            />
            {this.state.errors.manv !== "" ? (
              <div className="alert alert-danger">{this.state.errors.manv}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label>Tên nhân viên</label>
            <input
              type="text"
              className="form-control"
              name="tennv"
              onChange={this.handleOnChange}
              onBlur={this.handleErrors}
            />
            {this.state.errors.tennv !== "" ? (
              <div className="alert alert-danger">
                {this.state.errors.tennv}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control"
              name="email"
              onChange={this.handleOnChange}
              onBlur={this.handleErrors}
            />
            {this.state.errors.email !== "" ? (
              <div className="alert alert-danger">
                {this.state.errors.email}
              </div>
            ) : (
              ""
            )}
          </div>
          <button
            type="submit"
            className="btn btn-success"
            disabled={!this.state.formValid}
          >
            Add
          </button>
        </form>
        <ListUser/>;
      </div>
    );
  }
}


export default connect()(FormValidation);
