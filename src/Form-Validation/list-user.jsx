import React, { Component } from "react";
import { connect } from "react-redux";

class ListUser extends Component {
  renderEmp() {
    return this.props.employee.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.manv}</td>
          <td>{item.tennv}</td>
          <td>{item.email}</td>
          <td>
            <button className="btn btn-info">Edit</button>
          </td>
        </tr>
      );
    }, console.log(this.props.employee));
  }

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>MaNV</th>
              <th>TenNV</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderEmp()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    employee: state.formValidationReducer.empList,
  };
};
export default connect(mapStateToProps)(ListUser);
