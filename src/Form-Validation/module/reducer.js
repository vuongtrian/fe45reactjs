let initialState = {
    empList: []
}

const formValidationReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "ADD_EMP":
      state.empList.push(payload);
      return {...state};
    default:
      return state;
  }
};

export default formValidationReducer;
