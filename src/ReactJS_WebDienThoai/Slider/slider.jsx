import React, { Component } from "react";
import slider1 from "../../img/slide_1.jpg";
import slider2 from "../../img/slide_2.jpg";
import slider3 from "../../img/slide_3.jpg";

class Slider extends Component {
  render() {
    return (
      <div className="slider">
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to={0}
              className="active"
            />
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to={1}
              className
            />
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to={2}
              className
            />
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img className="d-block w-100" src={slider1} alt="First slide"  height={700}/>
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={slider2} alt="Second slide" height={700}/>
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={slider3} alt="Third slide" height={700}/>
            </div>
          </div>
          <a
            className="carousel-control-prev"
            href="#carouselExampleIndicators"
            role="button"
            data-slide="prev"
          >
            <span className="carousel-control-prev-icon" aria-hidden="true" />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            href="#carouselExampleIndicators"
            role="button"
            data-slide="next"
          >
            <span className="carousel-control-next-icon" aria-hidden="true" />
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
    );
  }
}

export default Slider;
