import React, { Component } from "react";
import Header from "../Header";
import Slider from "../Slider/slider";
import ProductList from "../ProductionList/productList";
import Footer from "../Footer";



class HomeBaiTapReact_1 extends Component {
  render() {
    return (
      <div className="Home bg-dark">
        <Header />
        <Slider/>
        <ProductList/>
        <Footer/>
      </div>
    );
  }
}

export default HomeBaiTapReact_1;
