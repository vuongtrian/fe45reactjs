let initialState = null;

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SELECTED_USER":
      return payload;
    default:
      return state;
  }
};

export default reducer;
