import React, { Component } from "react";
import Search from "../Search";
import UserList from "../UserList";
import Modal from "../Form";
import { connect } from "react-redux";


class Home_Rework extends Component {

handleAddUser = () => {
    this.props.dispatch({
        type: "SHOW_FORM",
      });
}

    
  render() {
    return (
      <div className="container">
        <h1 className="display-4 text-center my-3">User Management</h1>
        <div className="d-flex justify-content-between align-items-center">
         <Search />
          <button onClick={this.handleAddUser} className="btn btn-success">
            Add User
          </button>
        </div>
        <UserList/>
        {this.props.isShowModal && <Modal/>}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      isShowModal: state.isShow,
    };
  };

export default connect(mapStateToProps)(Home_Rework);
