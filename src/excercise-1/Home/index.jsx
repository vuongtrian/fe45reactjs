import React, { Component } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import Sidebar from '../Sidebar';
import Content from '../Content';
import './style.css';

class Home extends Component {
    render() {
        return (
            <div className="Home">
                <Header/>
                <div class='ex1-container'>
                    <Sidebar/>
                    <Content/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Home;